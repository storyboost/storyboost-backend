﻿using Microsoft.EntityFrameworkCore;
using Storyboost.WebApi.Models.Database;

namespace Storyboost.WebApi
{
    public class StoryboostContext : DbContext
    {
        public StoryboostContext(DbContextOptions<StoryboostContext> options)
            : base(options)
        {
        }

        public DbSet<DbProject> Projects { get; set; }

        public DbSet<DbProjectFile> ProjectFiles { get; set; }
    }
}
