﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Storyboost.WebApi.Models.Resources;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Storyboost.WebApi.Controllers
{
    [Route("api/project-files")]
    [ApiController]
    public class ProjectFilesController : ControllerBase
    {
        private readonly StoryboostContext _context;
        private readonly MinioContainer _minio;

        public ProjectFilesController(StoryboostContext context, MinioContainer minio)
        {
            _context = context;
            _minio = minio;
        }

        [HttpGet("entries")]
        public async Task<ActionResult> GetEntries([FromQuery] string projectId)
        {
            if (!Guid.TryParse(projectId, out Guid projectGuid))
            {
                return BadRequest();
            }

            var files = await _context.ProjectFiles
                .Where(f => f.ProjectId == projectGuid)
                .Select(f => new ProjectFileEntryResource
                {
                    Id = f.Id,
                    Name = f.Name,
                })
                .ToArrayAsync();

            return Ok(files);
        }

        [HttpGet("entries/{projectFileId}/download-link")]
        public async Task<ActionResult> GetEntryDownloadLink(string projectFileId)
        {
            if (!Guid.TryParse(projectFileId, out Guid projectFileGuid))
            {
                return BadRequest();
            }

            var projectFile = await _context.ProjectFiles.FindAsync(projectFileGuid);

            var url = await _minio.Client.PresignedGetObjectAsync(
                projectFile.StorageBucket,
                projectFile.StorageObject,
                // Lasts 1 hour
                60 * 60 * 1
            );

            return Ok(url);
        }
    }
}
