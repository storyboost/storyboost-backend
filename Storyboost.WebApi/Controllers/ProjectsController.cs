﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Storyboost.WebApi.Models.Resources;
using Storyboost.WebApi.Models.Database;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Storyboost.WebApi.Controllers
{
    [Route("api/projects")]
    [ApiController]
    public class ProjectsController : ControllerBase
    {
        private readonly StoryboostContext _context;
        private readonly MinioContainer _minio;

        public ProjectsController(StoryboostContext context, MinioContainer minio)
        {
            _context = context;
            _minio = minio;
        }

        [HttpGet("entries")]
        public ActionResult<IEnumerable<ProjectEntryResource>> GetEntries()
        {
            return Ok(_context.Projects.ToArray());
        }

        [HttpGet("entries/{projectId}")]
        public async Task<ActionResult> GetEntry(string projectId)
        {
            if (!Guid.TryParse(projectId, out Guid projectGuid))
            {
                return BadRequest();
            }

            var project = await _context.FindAsync<DbProject>(projectGuid);

            return Ok(new ProjectEntryResource
            {
                Id = project.Id,
                Name = project.Name,
            });
        }

        [HttpDelete("entries/{projectId}")]
        public async Task<ActionResult> DeleteEntry(string projectId)
        {
            if (!Guid.TryParse(projectId, out Guid projectGuid))
            {
                return BadRequest();
            }

            var project = await _context.FindAsync<DbProject>(projectGuid);
            _context.Projects.Remove(project);
            await _context.SaveChangesAsync();

            return Ok();
        }

        [HttpPost("entries/{projectId}/upload-file")]
        public async Task<ActionResult> UploadEntryFile(string projectId, [FromForm] IFormFile file)
        {
            if (!Guid.TryParse(projectId, out Guid projectGuid))
            {
                return BadRequest();
            }

            var fileName = Path.GetFileName(file.FileName);
            var stream = file.OpenReadStream();

            var bucketName = Guid.NewGuid().ToString();

            // Upload the file to a bucket
            await _minio.Client.MakeBucketAsync(bucketName);
            await _minio.Client.PutObjectAsync(bucketName, fileName, stream, file.Length);

            // Publish the file to the database
            var dbFile = new DbProjectFile
            {
                Id = Guid.NewGuid(),
                ProjectId = projectGuid,
                Name = fileName,
                StorageBucket = bucketName,
                StorageObject = fileName,
            };
            _context.ProjectFiles.Add(dbFile);
            await _context.SaveChangesAsync();

            return Ok();
        }

        [HttpPost("create")]
        public async Task<ActionResult> Create([FromBody] ProjectCreateResource project)
        {
            _context.Projects.Add(new DbProject
            {
                Id = Guid.NewGuid(),
                Name = project.Name,
            });
            await _context.SaveChangesAsync();

            return Ok();
        }
    }
}
