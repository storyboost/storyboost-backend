﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Storyboost.WebApi.Models.Database
{
    public class DbProject
    {
        public Guid Id { get; set; }

        [Required]
        public string Name { get; set; }

        public List<DbProjectFile> ProjectFiles { get; set; }
    }
}
