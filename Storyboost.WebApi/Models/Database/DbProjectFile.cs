﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Storyboost.WebApi.Models.Database
{
    public class DbProjectFile
    {
        public Guid Id { get; set; }
        
        [Required]
        public Guid ProjectId { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public string StorageBucket { get; set; }

        [Required]
        public string StorageObject { get; set; }

        public DbProject Project { get; set; }
    }
}
