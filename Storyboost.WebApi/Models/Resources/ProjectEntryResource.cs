﻿using System;
using System.Collections.Generic;

namespace Storyboost.WebApi.Models.Resources
{
    public class ProjectEntryResource
    {
        public Guid Id { get; set; }

        public string Name { get; set; }
    }
}
