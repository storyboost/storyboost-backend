﻿using System;

namespace Storyboost.WebApi.Models.Resources
{
    public class ProjectFileEntryResource
    {
        public Guid Id { get; set; }

        public string Name { get; set; }
    }
}
