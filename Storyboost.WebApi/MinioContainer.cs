﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Minio;

namespace Storyboost.WebApi
{
    public class MinioContainer
    {
        public MinioClient Client { get; set; }

        public MinioContainer(IConfiguration configuration, IWebHostEnvironment env)
        {
            Client = new MinioClient(
                configuration["StorageService:Endpoint"],
                configuration["StorageService:AccessKey"],
                configuration["StorageService:SecretKey"]
            );

            if (!env.IsDevelopment())
            {
                Client.WithSSL();
            }
        }
    }
}
