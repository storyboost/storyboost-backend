CREATE TABLE projects (
	id uuid primary key,
	name text not null
);

CREATE TABLE project_files (
	id uuid primary key,
	project_id uuid not null references projects(id),
	name text not null,
	storage_bucket text not null,
	storage_object text not null
);