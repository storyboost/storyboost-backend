# Storyboost Backend

## Development Environment

Create .env file in `Storyboost.WebApi`.

```
ConnectionStrings__StoryboostContext=Host=localhost;Database=storyboost;Username=postgres;Password=postgres
StorageService__Endpoint=127.0.0.1:9000
StorageService__AccessKey=ExampleAccessKey
StorageService__SecretKey=ExampleSecretKey
```

Create local postgres container.

```
docker network create dev-network
docker run --name dev-postgres -e POSTGRES_PASSWORD=postgres -p 5432:5432 --network dev-network -d postgres
```

Create local minio container.

```
docker run --name dev-minio -p 9000:9000 -e "MINIO_ACCESS_KEY=ExampleAccessKey" -e "MINIO_SECRET_KEY=ExampleSecretKey" --network dev-network -d minio/minio server /data
```

Create a new postgres database called "storyboost" and run `setup.sql` on it.


## pgAdmin Setup

```
docker run --name dev-pgadmin -p 80:80 -e 'PGADMIN_DEFAULT_EMAIL=postgres@example.com' -e 'PGADMIN_DEFAULT_PASSWORD=postgres' --network dev-network -d dpage/pgadmin4
```
